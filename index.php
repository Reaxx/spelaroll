﻿<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css" type='text/css'>
		<title>Spela Roll</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1><img src="./img/webbannertransp.png" id="titleimg"></h1>	
		</header>
		<aside>
		<section>
		<article>
			<header>
				<h2>Senaste nytt</h2>	
			</header>
			<p>
		<?php
			include "/home/virtual/spelaroll.eu/private_html/link.inc"; //DB connection
			$sql = "SELECT * FROM `event` WHERE `type` = 'news' AND `time` <= NOW() ORDER BY `time` DESC";			
			$result=mysqli_query($link,$sql);
			$news=mysqli_fetch_array($result,MYSQLI_ASSOC);
			
			if(!empty($news))
			{
				echo "<date class='kursiv'><b>".substr($news["time"],0,10).": </b></date>";
				echo utf8_encode($news["text"])."<br>";	
				echo "<date class='mini'>Senast ändrad: ".substr($news["entry"],0,16)."</date><br>";
				echo '<a href="schema.php?type=news" taget="_blank">Se alla</a>';
			}
			else echo 'Databasen är för stunden tom, för senaste nytt besök oss på <a href="https://www.facebook.com/groups/SpelaRoll/" target="_blank">Facebook</a>';
		?>
		
		
		</p>	
		</article>
		</section>
		<section>
			<header>
				<h2>Kommande:</h2>
			</header>
			
			<?php
			//Skriver ut nästkommande av varje eventtyp utom "news"
			$sql = "SELECT DISTINCT `type` FROM `event` WHERE `time` >= NOW() ORDER BY `time` ASC";
			$result=mysqli_query($link,$sql);	
			$n = 0;
			while($ttype = mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				$eventtype[$n] = $ttype["type"];
				$n++;
			}
			
			if(!empty($eventtype))
			{
			foreach ($eventtype as $type)
			{
				if($type != "news")
				{
					$sql = "SELECT * FROM `event` WHERE `type` = '".$type."' AND `time` >= NOW() ORDER BY `time` ASC";			
					$result=mysqli_query($link,$sql);
					$event=mysqli_fetch_array($result,MYSQLI_ASSOC);
			
					print '<article>
					<header>
					<h3>'.utf8_encode($event['title']).'</h3>	
					</header>
					<p>';
				
					if(!empty($event['text'])) echo utf8_encode($event['text'])."<br>";
					echo "<b>Tid: </b>".substr($event['time'],0,16)."<br>";
					echo "<b>Plats: </b>".utf8_encode($event['place'])."<br>";
					echo "<date class='mini'>Senast ändrad: ".substr($event["entry"],0,16)."</date><br>";
					
					switch ($event["type"])
					{
						case "bradspel":
							$typeTitle = "brädspel";
							break;
						case "rollspel":
							$typeTitle = "rollspel";
							break;
						case "magic":
							$typeTitle = "magic";
							break;
						case "ovrigt":
							$typeTitle = "'övrigt";
							break;
					}
					
					print '<a href="schema.php?type='.$event["type"].'">Se alla '.$typeTitle.'</a>
					</p>
					</article>';
				}
			}
			}
			else
			{
				print '<article>
				Just nu finns det inga inplanerade arrangemang, möt oss gärna på <a href="https://www.facebook.com/groups/SpelaRoll/" target="_blank">Facebook</a> om du är sugen på att få till spelande.
				</article>';
			}

		?>
		</section>
		</aside>
		<section class="main">
			<article>
				<figure id="arkhamhorror">
					<img src="./img/AH-Stefan.jpg" alt="Arkham Horror. Bild tagen av Stefan Björk-Olsén.">
					<figcaption>Arkham Horror. Bild tagen av Stefan Björk-Olsén.</figcaption>
				</figure>
				
				<header>
					<h2>Föreningen</h2>
				</header>			
			
				<p>
					<ul>
						<li>Brädspel, kortspel, figurspel, rollspel.</li>
						<li>De flesta speltillfällen är öppna även för icke-medlemmar (i mån av plats).</li>
					</ul>
				</p>
			
			</article>
			<article>
				<header>
					<h2>Kontakt</h2>	
				</header>
				<p>
					
					<address>
						Spela Roll<br>
						Tjärbyvägen 18<br>
						31235 Laholm<br>
					</address>
					För mer: <a href="https://www.facebook.com/groups/SpelaRoll/" target="_blank">Facebook</a>
				</p>
			</article>
		</section>
		
	<?php include './include/footer.inc'; ?>
	</body>
</html>

<?php
	mysqli_close($link);
?>