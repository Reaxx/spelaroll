<? include "/home/virtual/spelaroll.eu/private_html/link.inc"; //DB connection ?>
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="./../style.css">
		<script src="code.js"></script>
		<title>Spel - Filter</title>
	</head>
	<?php include '../include/nav.inc'; ?>
		<header class="title">
			<h1>Våra spel</h1>	
		</header>
		<aside>
			<article>
				<p>
				<form method="GET">
					<table>
					<tr>
						<td><label for="spelnamn">Spelnamn:</label></td>
						<td><input type="text" name="spelnamn" title="Titel på spel" <? echo 'value="'.$_GET['spelnamn'].'"'; ?> ></td>
					</tr><tr>
						<td><label for="spelare">Antal spelare:</label></td>
						<td><input type="number" name="spelare" <? echo 'value="'.$_GET['spelare'].'"'; ?> ></td>
					</tr><tr>
					<td><label for="tid">Maximal speltid:</label></td>
						<td><input type="number" name="tid" <? echo 'value="'.$_GET['tid'].'"'; ?>></td>
					</tr><tr>
					</tr><tr>
						<td><label for="co-op" title="Sammarbetsspel, med eller utan betrayer">Co-op:</label></td>
						<td><input type="checkbox" name="co-op" title="Sammarbetsspel, med eller utan betrayer" value=" AND `co-op` = 1" <? if(isset($_GET['co-op'])) { echo "checked"; } ?> ></td>
					</tr><tr>
						<td><label for="sort">Sortera på namn:</label></td>
						<td><input type="radio" name="sort" value="" <? if($_GET['sort'] == "") { echo "checked"; } ?>></td>
					</tr><tr>
						<td><label for="sort">Sortera på betyg:</label></td>
						<td><input type="radio" name="sort" value=" ORDER BY `medelbetyg` DESC" <? if($_GET['sort'] != "") { echo "checked"; } ?>></td>
					</tr><tr>
					</tr><tr>
						<td><label for="spelrum" title="Visa enbart spel som finns på plats i spelrummet">I spelrummet:</label></td>
						<td><input type="checkbox" title="Visa enbart spel som finns på plats i spelrummet" name="spelrum" value=" AND `ispelrum` = 1" <? if(!isset($_GET['open']) || !empty($_GET['spelrum'])) { echo "checked"; } ?>	></td>
					</tr><tr>
						<td><label for="showImg">Visa bilder:</label></td>
						<td><input type="checkbox" name="showImg" <? if(isset($_GET['showImg'])) { echo "checked"; } ?>></td>
					</tr><tr>
						<td colspan="2"><input type="submit" value="Sök" name="open"></td>
					</tr>
					</table>
				</form>
				</p>
			</article>
		</aside>
		<section class="main">
			<article>
				<p>
					<?
					if(isset($_GET['open']))
					{
						if(!empty($_GET['spelare']))
						{
							$spelare = " AND ".htmlentities($_GET['spelare'])." between `minspelare` AND `maxspelare`";
						}
						else { $spelare = "";}
						if(!empty($_GET['tid']))
						{
							$speltid = " AND ".htmlentities($_GET['tid'])." > `speltid`";
						}
						else { $speltid = "";}
						$sql = "SELECT *, COUNT(`betyg`) AS aBetyg, ROUND(avg(betyg),1) as `medelbetyg` 
								FROM spel
								LEFT JOIN betyg
								ON spel.spelnamn=betyg.spelnamn
								WHERE spel.`spelnamn` LIKE '%".htmlentities($_GET['spelnamn'])."%'
								".$spelare."
								".$speltid."
								".$_GET['spelrum']."
								".$_GET['co-op']."
								GROUP BY spel.spelnamn".$_GET['sort'];								
						
						$result=mysqli_query($link,$sql);
						$lines = mysqli_num_rows($result);
						$open = "";
						$showImg = "0";
						
						if($lines <= 8)
						{
							$open = " open";
							$showImg = 1;
						}
						if(isset($_GET['showImg']))
						{
							$showImg = 1;
							$open = " open";
						}
			
						
						while ($spel=mysqli_fetch_array($result,MYSQLI_BOTH))
						{
							
							if(empty($spel['medelbetyg']))	//Work-around för fel jag inte ritkigt förstår, där speltitel inte ges om inte några betyg är satta.
							{
								$spel['spelnamn'] = $spel[1];
								$spel['medelbetyg'] = "-";
							}
							
							if(empty($spel['beskrivning']))
							{
								$spel['beskrivning'] = 'Se <a href="'.$spel["lank"].'" target="_blank">BGG</a>';
							}
							if($spel['ispelrum'] == 1)
							{
								$titelrad='<b>'.$spel['spelnamn'].' </b>   '.$spel['medelbetyg'];
							}
							else
							{
								$titelrad='<I>'.$spel["spelnamn"].' </I>   '.$spel['medelbetyg'];
							}
							if (!empty($spel["lank"]))
							{
								$lank = '<a href="'.$spel["lank"].'" title="Boardgamegeek" target="_blank"><img src="'.getThumbUrl($spel["lank"],$showImg).'" alt="Thumbnail" height="70em" width="70em"></a>';
							}
							else
							{
								$lank = "";
							}
							
							$coop = "Nej";
							if(($spel['co-op']) == 1)
							{
								$coop = "Ja";
							}
							
							
							if(!empty($spel['regler']))
							{
								$regler = '<td><a href="'.$spel['regler'].'" title="Spelregler" target="_blank">Regler</a></td>';
							}
							else
							{
								$regler = "";
							}
							
							print '
							<details'.$open.'>
								<summary>
								<spacetext>
								'.$titelrad.'
								</spacetext>
								</summary>
								<table class="borderless">
								<tr><td rowspan="4">
								'.$lank.'
								</tr></td>
								<tr>
									<td><b>Spelare:</b> '.$spel['minspelare'].'-'.$spel['maxspelare'].'</td>
									<td><b>Speltid:</b> '.$spel['speltid'].'min</td>
									<td><b>Co-op:</b> '.$coop.'</td>
									<td><b>Betyg:</b> '.$spel['medelbetyg'].' <small title="Antal röster">('.$spel['aBetyg'].')</small></td>
									'.$regler.'
								</tr>
								<tr>
									<td colspan="5"><b>Beskrivning:</b> '.$spel['beskrivning'].'</td>			
								</tr>
								<tr>
									<td colspan="2"><small><b>Ägare:</b> '.$spel['agare'].'</small></td>	
									<td colspan="2"><small><b>Senast ändrad:</b> '.substr($spel['editdatum'],0,11).'</small></td>	
								</tr>
								</table>
							</details>';
						}
						$sql = "SELECT COUNT(*) FROM `spel`";								
						$result=mysqli_query($link,$sql);
						$total=mysqli_fetch_array($result,MYSQLI_NUM);
						print 
						'</p>
						<p><small>'.$lines. ' träffar av '.$total[0].'</small></p>';
						//echo $sql; 		//Felsökningc
					}
					else
					{
						echo "<p>Använd kontrollerna till höger för att göra din sökning.</p>";
					}
					?>				
			</article>
		</section>
		<?php include '../include/footer.inc'; ?>
	</body>
</html>
<?
/** Hämtar BGGs thumb-nail utifrån länk **/
function getThumbUrl($url,$showImg)	//Lines räknar rader som hämtas från databasen.
{
	if($showImg == 1)
	{
	//Plockar fram IDt från BGG-länk 
	//Exempel: https://boardgamegeek.com/boardgame/68448/7-wonders
	$url = explode ("/",$url);
	$id = $url[4];

	//Hämtar information om spelet från BGG
	$curl = curl_init("https://www.boardgamegeek.com/xmlapi/boardgame/".$id);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	
	//Plockar ut thumb-nail URLen från BGG
	$imgUrl = explode ("//",$result);
	$parsedImgUrl = strip_tags($imgUrl[2]);
	$parsedImgUrl = chop($parsedImgUrl);
	return "http://".$parsedImgUrl;
	}
}

?>