function loadBackground()
{
    if(localStorage.selectedBackgroundImage)
    {
        var selectedBackgroundImage = localStorage.selectedBackgroundImage;
        opts = document.getElementById("backgroundImageSelect").options;
        for(var opt, j = 0; opt = opts[j]; j++) {
            if(opt.value == selectedBackgroundImage) {
                document.getElementById("backgroundImageSelect").selectedIndex = j;
                image(document.getElementById("backgroundImageSelect"));
                break;
            }
        }
    }
}

function image(incoming)
{
    if(incoming.value==0)
    {
        document.body.style.backgroundImage = "url()";
        document.body.style.backgroundColor = "#FFFFFF";
    }
    else
    {
        document.body.style.backgroundColor = "#000000";
        document.body.style.backgroundImage = "url(./images/"+incoming.value+".jpg)";
        document.body.style.backgroundRepeat = "no-repeat";
        document.body.style.backgroundSize= "cover";
        document.body.style.backgroundPosition = "center center";
    }
    localStorage.selectedBackgroundImage = incoming.value;
}

function timeAll()
{
    var timestamps = document.getElementsByClassName("timestamp");
    for(var i = 0; i < timestamps.length; i++)
    {
        timestamps[i].innerHTML = timeAgo(timestamps[i].innerHTML);
        timestamps[i].style.display = "inline";
    }
}