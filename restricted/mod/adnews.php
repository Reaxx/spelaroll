
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../../style.css">
		<script src="code.js"></script>
		<title>Spela Roll - AdNews</title>
	</head>
	<body>
		<?
						$news["time"] = date("Y-m-d H:i:s");	//Sätter time() som föreslaget publiceringsdatum
		
						include "../../../private_html/link.inc";
						$title = htmlentities($_POST['title']);
						$text = htmlentities($_POST['text']);
						$time = htmlentities($_POST['time']);
						$key = htmlentities($_POST['key']);
						
						if(isset($_POST['upp']) && isset($_POST['key']))
						{
							//Uppdaterar existerande entry
							$sql = "UPDATE `event` SET `title` = '".$title."', `text` = '".$text."', `time` = '".$time."' WHERE `event`.`key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['upp']))
						{
							//Skapar nytt entry
							$sql = "INSERT INTO `event` (`key`, `entry`, `title`, `text`, `type`, `place`, `time`) VALUES (NULL, CURRENT_TIMESTAMP, '".$title."', '".$text."', 'news', '', '".$time."')";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['del']))
						{
							//Raderar entry
							
							$key = $_POST['oldNews'];	
							$sql = "DELETE FROM `event` WHERE `event`.`key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
					?>
	
		<?php include '../../include/nav.inc'; ?>
		
		<header class="title">
			<h1>Administrera nyheter</h1>	
		</header>
		<aside>
		<?
			
			$sql = 'SELECT * FROM `event` WHERE `type` = "news" ORDER BY `time` DESC';		
			$result=mysqli_query($link,$sql);
			
			print "<form method='post'>
			<select name='oldNews' size='10'>";
			
			while ($oldNews=mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				echo "<option value='".$oldNews['key']."'>".substr($oldNews["time"],0,10)." - ".utf8_encode($oldNews['title'])."</option>";
			}
			print "</select>
			<br>
			<input type='submit' value='Öppna' name='oppna'>
			<input type='submit' value='Radera' name='del' onclick=\"return confirm('Du raderar ett inlägg, vill du fortsätta?')\" >
			</form>";
			
			
		?>
		</aside>
		<section class="main">
			<article>
				<p>
					<?
						print '<form method="post">
						<table>';
												
						if(isset($_POST['oppna']) && !empty($_POST['oldNews']))
						{
							$sql = 'SELECT * FROM `event` WHERE `key` = "'.$_POST['oldNews'].'"';
							$result=mysqli_query($link,$sql);
							$news=mysqli_fetch_array($result,MYSQLI_ASSOC);
						
							echo '<input type="hidden" name="key" value="'.$news["key"].'">';
							echo "<tr><td colspan='2'><small>Senast ändrad: ".$news["entry"]."</small></tr></td>";
							
						}
						print '
						<tr>
						<td><label for="title">Title</label></td>
						<td><input type="text" name="title" value="'.utf8_encode($news["title"]).'"></td>
						</tr><tr>
						<td><label for="publication">Publicering</label></td>
						<td><input type="text" name="time" value="'.date($news["time"]).'"></td>
						</tr><tr>
						<td><label for="text">Text</label></td>
						</tr><tr>
						<td colspan="2"><textarea name="text" rows="10" cols="50">'.utf8_encode($news["text"]).'</textarea><br><td>
						</tr><tr>
						<td colspan="2"><input type="submit" value="Skapa/Uppdatera" name="upp"></td>
						</tr>
						
						</table></form>';
					?>
				
				</p>
			</article>
		</section>
		<?php include '../../include/footer.inc'; ?>
	</body>
</html>
