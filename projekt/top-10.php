<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/style.css">
		<script src="code.js"></script>
		<title>Template</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header id="title">
			<h1>Top 10</h1>
		</header>
		<section class="main">
			<article>
				<p>
					<table>
					<?
						 $data = file("https://docs.google.com/spreadsheets/d/1EFl-epraTTGMyGwl7a5Y7sXv7c_y5rumfPtiSdY7Wi4/pub?gid=1721038696&single=true&output=csv");
						array_shift($data);
						
						foreach ($data as $text)
						 {
							echo "<tr>";
							echo "<td>".explode(",",$text)[0]."</td>";
							echo "<td>".explode(",",$text)[1]."</td>";
							echo "</tr>";
						}
					?>
					</table>
				</p>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
