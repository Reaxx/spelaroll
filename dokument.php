<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<title>Filer</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Filer</h1>	
		</header>
		<table>
			<thead>
				<th>Namn</th>
				<th>Upplagd</th>
				<th>Beskrivning</th>
			</thead>
			<tbody>
				<tr>
					<td><a href="">Stadgar</a></td>
					<td>2016-02-15</td>
					<td>Gällande stadgar för föreningen</td>
				</tr>
				<tr>
					<td><a href="./filer/arsmote-2016-02-16.pdf" target="_blank">Årsmötesprotokoll 2016</a></td>
					<td>2016-04-02</td>
					<td>Årsmöte för 2016</td>
				</tr>
				<tr>
					<td><a href="./filer/arsmote-2015-05-20.pdf" target="_blank">Årsmötesprotokoll 2015</a></td>
					<td>2016-02-07</td>
					<td>Uppstarts- och årsmöte för 2015</td>
				</tr>
			</tbody>
		</table>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
