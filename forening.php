<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<script src="code.js"></script>
		<title>Template</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Om Spela Roll</h1>	
		</header>
		<aside>
			<article>
				<article>
				<header>
					<h2>Styrelsen</h2>	
				</header>
				<p>
				<table>
					<tbody>
						<tr>
							<td>Ordförande:</td>
							<td>Stefan Björk-Olsén</td>
						</tr>
						<tr>
							<td>Sekreterare:</td>
							<td>Kristian Nilsson</td>
						</tr>
						<tr>
							<td>Kassör:</td>
							<td><address><a href="mailto:swe_jonny@hotmail.com">Jonny Svensson</a></address></td>
						</tr>
						<tr>
							<td>Revisor:</td>
							<td>Mikael Bylund</td>
						</tr>				
					</tbody>					
				</table>
				</p>
			
			</article>
					<header>
						<h2>Kontakt</h2>	
					</header>
					<p>
						<address>
							Spela Roll<br>
							Tjärbyvägen 18<br>
							31235 Laholm<br>
						</address>
						För mer se <a href="https://www.facebook.com/groups/SpelaRoll/" target="_blank">facebook</a>
					</p>
			</article>
		</aside>
		<section class="main">	
			<article>
				<header>
					<h2>Verksamhet</h2>
				</header>
				<p>
					Vi är en lite
				</p>
			</article>
			<article>
				<header>
					<h2>Medlemskap</h2>
				</header>
				<p>
					<ul>
						<li>Merparten av våra arrangemang är öppna för icke-medlemmar</li>
						<li>Medlemmar</li>
						<ul>
							<li>Avgift: 250kr</li>
							<li>Rösträtt om spelinköp</li>
							<li>Har rätt att låna hem föreningens spel</li>
							<li>Rösträtt på årsmöte</li>
						</ul>
						<li>Distansmedlemmar</li>
						<ul>
							<li>Avgift: 100kr</li>
							<li>Något begränsad rätt att rösta på spelinköp</li>
							<li>Har inte rätt att låna hem föreningens spel</li>
							<li>Rösträtt på årsmöte</li>
						</ul>
					</ul>
				</p>
				<p>
					Clr: 8183-6 <br>
					Konto: 303 038 596-1 <br>
					<a href="http://l.facebook.com/l.php?u=http%3A%2F%2Febas.sverok.se%2Fsignups%2Findex%2F9022&h=tAQGb9u1w&s=1" target="_blank">Bli medlem</a>
				</p>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
