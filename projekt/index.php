<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/style.css">
		<script src="code.js"></script>
		<title>Projekt</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Projekt</h1>	
		</header>
		<section class="main">
			<article>
				<p>
					Projekt, kodsnuttar och olika tester i olika grader av funktionsduglighet.
				</p>
				<p>
					<table>
			<thead>
				<th>Namn</th>
				<th>Beskrivning</th>
				<th>Ägare</th>
				<th>Skick</th>
			</thead>
			<tbody>
				<tr>
					<td><a href="spel.php">Spel</a></td>
					<td>Listar spel från google docs</td>
					<td>Jonny</td>
					<td>Fungerar</td>
				</tr>
				<tr>
					<td><a href="top-10.php">Top 10</a></td>
					<td>Listar top 10 från google docs</td>
					<td>Jonny</td>
					<td>Fungerar</td>
				</tr>
				<tr>
					<td><a href="bggApi.php">BggApi</a></td>
					<td>BoardgameGeek API-test</td>
					<td>Jonny</td>
					<td>Fungerande</td>
				</tr>
				<tr>
					<td><a href="ranking.php">Ranking</a></td>
					<td>System för att ranka föreningens inköp.</td>
					<td>Jonny</td>
					<td>Påbörjad</td>
				</tr>
				<tr>
					<td><a href="rapport.php">Rapport</a></td>
					<td>Rapportsystem för event.</td>
					<td>Jonny</td>
					<td>Påbörjad</td>
				</tr>
				
			</tbody>
		</table>
				</p>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
