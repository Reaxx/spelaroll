	<?
						$indatum = date("Y-m-d");	//Sätter time() som föreslaget publiceringsdatum
						$key = htmlentities($_POST['key']);
						$spelarnamn = htmlentities($_POST['spelarnamn']);
						$avgift = htmlentities($_POST['avgift']);
						$user = htmlentities($_POST['user']);
						
						//$losenord = htmlentities($_POST['losenord']);
						$email = htmlentities($_POST['email']);

						include "../../../private_html/link.inc";
						
						if(isset($_POST['upp']) && isset($_POST['key']))
						{
							//Uppdaterar existerande entry
							$sql = "UPDATE `deltagare` SET `spelarnamn` = '".$spelarnamn."', `email` = '".$email."', `avgift` = '".$avgift."', `user` = '".$user."' WHERE `key` = ".$key;
							//echo "UPDATE `deltagare` SET `spelarnamn` = '".$spelarnamn."', `email` = '".$email."' WHERE `key` = ".$key;
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['upp']))
						{
							//Skapar nytt entry
							$sql = "INSERT INTO `deltagare` (`spelarnamn`, `avgift`, `editdatum`, `indatum`, `email`, `user`) VALUES ('".$spelarnamn."', '".$avgift."', CURRENT_TIMESTAMP, '".$indatum."', '".$email."', '".$user."')";
							//echo "INSERT INTO `deltagare` (`spelarnamn`, `medlem`, `editdatum`, `indatum`, `email`) VALUES ('".$spelarnamn."', '".$medlem."', CURRENT_TIMESTAMP, '".$indatum."', '".$email."')";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['del']))
						{
							//Raderar entry
							
							$key = $_POST['deltagare'];	
							$sql = "DELETE FROM `deltagare` WHERE `key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
					?>
					
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../../style.css">
		<script src="code.js"></script>
		<title>AdDeltagare</title>
	</head>
	<body>
		<?php include './../../include/nav.inc'; ?>
		
		<header class="title">
			<h1>Administrera Deltagare</h1>	
		</header>
		<aside>
		<?
			//Generar lista
			print "<form method='post'>
			<select name='deltagare'>";
			
			
			//Håller öppen vid uppdatering
			if(isset($_POST['upp']) || isset($_POST['oppna']))
			{
				if(isset($_POST['oppna']))
				{
					$sql = 'SELECT * FROM `deltagare` WHERE `key` = '.$_POST['deltagare'];
				}
				else
				{
					$sql = 'SELECT * FROM `deltagare` WHERE `key` = '.$_POST['key'];
				}
										
				$result=mysqli_query($link,$sql);
				while ($deltagare=mysqli_fetch_array($result,MYSQLI_ASSOC))
				{
					echo "<option value='".$deltagare['key']."'>".utf8_encode($deltagare['spelarnamn'])."</option>";
				}
			}
			

			$sql = 'SELECT * FROM `deltagare` ORDER BY `spelarnamn` ASC';		
			$result=mysqli_query($link,$sql);
		
			while ($deltagare=mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				echo "<option value='".$deltagare['key']."'>".utf8_encode($deltagare['spelarnamn'])."</option>";
			}
			print "</select>
			<br>
			<input type='submit' value='Öppna' name='oppna'>
			<input type='submit' value='Radera' name='del' onclick=\"return confirm('Du raderar ett spel, vill du fortsätta?')\" >
			</form>";
			
			
		?>
		</aside>
		<section class="main">
			<article>
				<p>
					<?

						print '<form method="post">
						<table>';
												
						if(isset($_POST['oppna']) && !empty($_POST['deltagare']) || isset($_POST['upp']))
						{
							
							if(isset($_POST['upp']))
							{
								//Håller informationen öppen efter uppdatering.
								$sql = 'SELECT * FROM `deltagare` WHERE `spelarnamn` = "'.$_POST['spelarnamn'].'"';
							}
							else 
							{
								$sql = 'SELECT * FROM `deltagare` WHERE `key` = "'.$_POST['deltagare'].'"';
							}
							
							$result=mysqli_query($link,$sql);
							$showdeltagare=mysqli_fetch_array($result,MYSQLI_ASSOC);
						
							echo '<input type="hidden" name="key" value="'.$showdeltagare["key"].'">';
							echo "<tr><td><small>Senast ändrad:</small></td><td><small>".$showdeltagare["editdatum"]."</small></tr></td>";
							
						}

						print '
						<tr>
						<td><label for="title">Namn:</label></td>
						<td><input type="text" name="spelarnamn" value="'.utf8_encode($showdeltagare["spelarnamn"]).'" required></td>
						</tr><tr>
						<td><label for="title">Användarnamn:</label></td>
						<td><input type="text" name="user" value="'.utf8_encode($showdeltagare["user"]).'" required></td>
						</tr><tr>
						<td><label for="title">Lösenord:</label></td>
						<td><input type="password" name="losenord"  value="'.utf8_encode($showdeltagare["losenord"]).'" disabled></td>
						</tr><tr>
						<td><label for="title">Email:</label></td>
						<td><input type="email" name="email"  value="'.utf8_encode($showdeltagare["email"]).'"></td>
						</tr><tr>
						<td><label for="title">Avgift (kr):</label></td>
						<td><input type="number" name="avgift" value="'.utf8_encode($showdeltagare["avgift"]).'">kr</td>
						</tr><tr>
						<td colspan="2"><input type="submit" value="Skapa/Uppdatera" name="upp"></td>
						</tr>						
						</table></form>';
					?>
				
				</p>
			</article>
		</section>
		<?php include './../../include/footer.inc'; ?>
	</body>
</html>
