<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<script src="code.js"></script>
		<title>Arrangemang</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Arangemang</h1>	
		</header>
		
		<aside>
		<article id="top10">
			<header>
				<h2>Top 10 brädspel</h2>	
			</header>
			<article>
				<table id="top10table">
					<?
						 $data = file("https://docs.google.com/spreadsheets/d/1EFl-epraTTGMyGwl7a5Y7sXv7c_y5rumfPtiSdY7Wi4/pub?gid=1721038696&single=true&output=csv");
						array_shift($data);
						
						foreach ($data as $text)
						 {
							echo "<tr>";
							echo "<td>".explode(",",$text)[0]."</td>";
							echo "<td>".explode(",",$text)[1]."</td>";
							echo "</tr>";
						}
					?>
				</table>
			</article>
		</aside>
		
		<section class="main">
			<article id="bradspel">
				<header>
					<h2>Brädspel</h2>
				</header>
				<p>
					Brädspel<br>
					
					<a href="filter.php" target="_blank">Tillgängliga spel</a>					
				</p>
			</article>
			<article id="rollspel">
				<header>
					<h2>Rollspel</h2>
				</header>
				<p>
					Rollspel
				</p>
			</article>
			<article id="magic">
				<header>
					<h2>Magic</h2>
				</header>
				<p>
					Just nu inget planerat
				</p>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
