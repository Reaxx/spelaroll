<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<script src="code.js"></script>
		<title>Template</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Schema</h1>	
		</header>
		<aside>
			<article>
				<p>
					Allt över linjen anger framtida arrangemang, medan allt under är redan inträffade.
				</p>
			</article>
		</aside>
		
		<section class="main">
			<article>
				<p>
					<? 
					include "../private_html/link.inc"; //Databaskoppling
					
				
					$sql = "SELECT * FROM `event` WHERE `type` = '".$_GET['type']."' AND `time` >= NOW() ORDER BY `time` DESC";
					
					$result=mysqli_query($link,$sql);	
					$n = 0;
				
					while($event = mysqli_fetch_array($result,MYSQLI_ASSOC))
					{
						print '<article>
						<header>
						<h3>'.utf8_encode($event['title']).'</h3>	
						</header>
						<p>';
					
						if(!empty($event['text'])) { echo utf8_encode($event['text'])."<br>"; }
						echo "<b>Tid: </b>".substr($event['time'],0,16)."<br>";
						echo "<b>Plats: </b>".utf8_encode($event['place'])."<br>";
						echo "<date class='mini'>Senast ändrad: ".substr($event["entry"],0,16)."</date><br>";
						print '</p>
						</article>';
						}
					
					?>
				</p>
			</article>
			<hr>
			<article>
				<p>
					<? 
				
					$sql = "SELECT * FROM `event` WHERE `type` = '".$_GET["type"]."' AND `time` <= NOW() ORDER BY `time` DESC";
					
					$result=mysqli_query($link,$sql);	
					$n = 0;
				
					while($event = mysqli_fetch_array($result,MYSQLI_ASSOC))
					{
						print '<article>
						<header>
						<h3>'.utf8_encode($event['title']).'</h3>	
						</header>
						<p>';
					
						if(!empty($event['text'])) { echo utf8_encode($event['text'])."<br>"; }
						echo "<b>Tid: </b>".substr($event['time'],0,16)."<br>";
						echo "<b>Plats: </b>".utf8_encode($event['place'])."<br>";
						echo "<date class='mini'>Senast ändrad: ".substr($event["entry"],0,16)."</date><br>";
						print '</p>
						</article>';
						}
					
					?>
				</p>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
<?php
	mysqli_close($link);
?>