
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../../style.css">
		<script src="code.js"></script>
		<title>Administrera Event</title>
	</head>
	<body>
		<?
						$news["time"] = date("Y-m-d H:i:s");	//Sätter time() som föreslaget publiceringsdatum
		
						include "../../../private_html/link.inc"; //Databaskoppling
						$title = htmlentities($_POST['title']);
						$text = htmlentities($_POST['text']);
						$time = htmlentities($_POST['time']);
						$key = htmlentities($_POST['key']);
						$type = htmlentities($_POST['type']);
						$place = htmlentities($_POST['place']);
						
						if(isset($_POST['upp']) && isset($_POST['key']))
						{
							//Uppdaterar existerande entry
							$sql = "UPDATE `event` SET `title` = '".$title."', `text` = '".$text."', `time` = '".$time."', `type` = '".$type."', `place` = '".$place."' WHERE `event`.`key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['upp']))
						{
							//Skapar nytt entry
							$sql = "INSERT INTO `event` (`key`, `entry`, `title`, `text`, `place`, `time`, `type`) VALUES (NULL, CURRENT_TIMESTAMP, '".$title."', '".$text."', '".$place."', '".$time."','".$type."')";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['del']))
						{
							//Raderar entry
							
							$key = $_POST['oldNews'];	
							$sql = "DELETE FROM `event` WHERE `event`.`key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
					?>
	
		<?php include '../../include/nav.inc'; ?>
		
		<header class="title">
			<h1>Administrera Event</h1>	
		</header>
		<aside>
		<?
			/** Generar lista över existerande event**/
			$sql = 'SELECT * FROM `event` WHERE `type` != "news" ORDER BY `time` DESC';		//News används specefikt för news.
			$result=mysqli_query($link,$sql);
			
			print "<form method='post'>
			<label for='oldNews'><b>Event i systemet:</b></label><br>
			<select name='oldNews' size='10'>";
			
			while ($oldNews=mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				echo "<option value='".$oldNews['key']."'>".substr($oldNews["time"],0,10)." - ".utf8_encode($oldNews['title'])."</option>";
			}
			print "</select>
			<br>
			<input type='submit' value='Öppna' name='oppna'>
			<input type='submit' value='Radera' name='del' onclick=\"return confirm('Du raderar ett inlägg, vill du fortsätta?')\" >
			</form>";
			
			
		?>
		</aside>
		<section class="main">
			<article>
				<p>
					<?
						/** Genear formuläret, med inmatning om sådant finns **/
						print '<form method="post">
						<table>';
												
						/** Hämtar event från databasen **/						
						if(isset($_POST['oppna']) && !empty($_POST['oldNews']))
						{
							$sql = 'SELECT * FROM `event` WHERE `key` = "'.$_POST['oldNews'].'"';
							$result=mysqli_query($link,$sql);
							$news=mysqli_fetch_array($result,MYSQLI_ASSOC);
						
							echo '<input type="hidden" name="key" value="'.$news["key"].'">';	//Skickar med key för hantering efter omladdning
							echo "<tr><td colspan='2'><small>Senast ändrad: ".$news["entry"]."</small></tr></td>";
							
						}
						print '
						<tr>
						<td><label for="title">Title</label></td>
						<td><input type="text" name="title" value="'.utf8_encode($news["title"]).'"></td>
						</tr><tr>
						<td><label for="time">Tid:</label></td>
						<td><input type="text" name="time" value="'.date($news["time"]).'"></td>
						</tr><tr>
						<td><label for="place">Plats:</label></td>
						<td><input type="text" name="place" value="'.utf8_encode($news["place"]).'"></td>
						</tr><tr>
						<td><label for="typ">Typ:</label></td>
						<td>'.typer($news['type']).'</td>
						</tr><tr>
						<td class="borderless"><label for="text">Text</label></td>
						</tr><tr>
						<td colspan="2"><textarea name="text" rows="10" cols="50">'.utf8_encode($news["text"]).'</textarea><br><td>
						</tr><tr>';
						
						//På Robins begäran, gör det tydligare om det är ett nytt entry eller uppdaterande av existerande.
						if(isset($_POST['oppna']))
						{
							echo '<td colspan="2"><input type="submit" value="Uppdatera" name="upp"></td>';
						}
						else
						{
							echo '<td colspan="2"><input type="submit" value="Skapa" name="upp"></td>';
						}
						
						echo '</tr></table></form>';
					?>
				
				</p>
			</article>
		</section>
		<?php include '../../include/footer.inc'; ?>
	</body>
</html>

<?
	/** Generar rulllista med rätt alternativ förvalt **/
	function typer($vald)
	{		
		$output = "<select name='type'>";
		
		/** Lägger förvald först **/
		switch ($vald)
		{
			case "bradspel":
				$output .= "<option value='bradspel'>Brädspel</option>";
				break;
			case "rollspel":
				$output .= "<option value='rollspel'>Rollspel</option>";
				break;
			case "magic":
				$output .= "<option value='magic'>Magic</option>";
				break;
			case "ovrigt":
				$output .= "<option value='ovrigt'>Övrigt</option>";
				break;
		}
		
		/** Skapar resten av listan **/
		$output .= "<option value='bradspel'>Brädspel</option>";
		$output .= "<option value='rollspel'>Rollspel</option>";
		$output .= "<option value='magic'>Magic</option>";
		$output .= "<option value='ovrigt'>Övrigt</option>";
		$output .= "</select>";
		
		return $output;
	}
?>