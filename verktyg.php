<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<title>Verktyg</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Verktyg</h1>	
		</header>
		<aside>
			<article>
				<p>
					Verktyg för att underlätta alla typer av spelande. 
				</p>
			</article>
		</aside>
		<table>
			<thead>
				<th>Namn</th>
				<th>Beskrivning</th>

			</thead>
			<tbody>
				<tr>
					<td><a href="http://spelaroll.eu/l5r/" target="_blank">L5R Dice Generator</a></td>
					<td>Tärningsgenerator specialskriven för Legend of the Five Rings</td>
				</tr>
				<tr>
					<td><a href="http://hourglass.nu/roll" target="_blank">Dice Roller</a></td>
					<td>Tärningsgenerator med fokus på WoD, men som fungerar även för generiska tärningsslag.</td>
				</tr>
				<!-- <tr>
					<td><a href="http://www.pms.nu/spela_roll/projekt/namegen/index.php" target="_blank">NameGen</a></td>
					<td>Namngenerator för ett stort antal nationaliteter.</td>
				</tr> -->
				<tr>
					<td><a href="http://spelaroll.eu/projekt/missions/" target="_blank">Kill Team Missionselector</a></td>
					<td>Verktyg för att slumpa fram ett uppdrag till Kill Team givet ett antal filter.</td>
				</tr>
			</tbody>
		</table>
		<?php include './include/footer.inc'; ?>
	</body>
</html>
