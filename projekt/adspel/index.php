
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../../style.css">
		<script src="code.js"></script>
		<title>AdSpel</title>
	</head>
	<body>
		<?
						$indatum = date("Y-m-d");	//Sätter time() som föreslaget publiceringsdatum
		
						include "../../../private_html/link.inc";
						$spelnamn = htmlentities($_POST['spelnamn']);
						$lank = $_POST['lank'];	//Osäker på hur den bör escpas
						$minspelare = htmlentities($_POST['minspelare']);
						$maxspelare = htmlentities($_POST['maxspelare']);
						$agare = htmlentities($_POST['agare']);
						$speltid = htmlentities($_POST['speltid']);
						$agare = htmlentities($_POST['agare']);
						$typ = htmlentities($_POST['typ']);
						
						if(isset($_POST['ispelrum'])) { $ispelrum = 1; }
						else {$ispelrum = 0;}
						if(isset($_POST['co-op'])) { $coop = 1; }
						else {$coop = 0;}
						
						$key = htmlentities($_POST['key']);
						
						if(isset($_POST['upp']) && isset($_POST['key']))
						{
							//Uppdaterar existerande entry
							$sql = "UPDATE `spel` SET `spelnamn` = '".$spelnamn."', `lank` = '".$lank."', `minspelare` = '".$minspelare."', `maxspelare` = '".$maxspelare."', `speltid` = '".$speltid."', `co-op` = '".$coop."', `agare` = '".$agare."', `ispelrum` = '".$ispelrum."', `typ` = '".$typ."' WHERE `spel`.`key` = ".$key;
							//echo "UPDATE `spel` SET `spelnamn` = '".$spelnamn."', `lank` = '".$lank."', `minspelare` = '".$minspelare."', `maxspelare` = '".$maxspelare."', `speltid` = '".$speltid."', `co-op` = '".$coop."', `agare` = '".$agare."', `ispelrum` = '".$ispelrum."', `typ` = '".$typ."' WHERE `spel`.`key` = ".$key;
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['upp']))
						{
							//Skapar nytt entry
							$sql = "INSERT INTO `spel` (`spelnamn`, `lank`, `minspelare`, `maxspelare`, `speltid`, `co-op`, `agare`, `ispelrum`, `typ`, `editdatum`, `indatum`) VALUES ('".$spelnamn."', '".$lank."', '".$minspelare."', '".$maxspelare."', '".$speltid."', '".$coop."', '".$agare."', '".$ispelrum."', '".$typ."', CURRENT_TIMESTAMP, '".$indatum."')";
							//echo "INSERT INTO `spel` (`spelnamn`, `lank`, `minspelare`, `maxspelare`, `speltid`, `co-op`, `agare`, `ispelrum`, `typ`, `editdatum`, `indatum`) VALUES ('".$spelnamn."', '".$lank."', '".$minspelare."', '".$maxspelare."', '".$speltid."', '".$coop."', '".$agare."', '".$ispelrum."', '".$typ."', CURRENT_TIMESTAMP, '".$indatum."')";
							$result=mysqli_query($link,$sql);
						}
						elseif(isset($_POST['del']))
						{
							//Raderar entry
							
							$key = $_POST['game'];	
							$sql = "DELETE FROM `spel` WHERE `key` = '".$key."'";
							$result=mysqli_query($link,$sql);
						}
					?>
	
		<?php include '../include/nav.inc'; ?>
		
		<header class="title">
			<h1>Administrera Spel</h1>	
		</header>
		<aside>
		<?
			//Generar lista
			print "<form method='post'>
			<select name='game'>";
			
			//Håller öppen vid uppdatering
			if(isset($_POST['upp']) || isset($_POST['oppna']))
			{
				if(isset($_POST['oppna']))
				{
					$sql = 'SELECT * FROM `spel` WHERE `key` = '.$_POST['game'];
				}
				else
				{
					$sql = 'SELECT * FROM `spel` WHERE `key` = '.$_POST['key'];
				}
				
						
				$result=mysqli_query($link,$sql);
		
				while ($games=mysqli_fetch_array($result,MYSQLI_ASSOC))
				{
					echo "<option value='".$games['key']."'>".utf8_encode($games['spelnamn'])."</option>";
				}
			}


			$sql = 'SELECT * FROM `spel` ORDER BY `spelnamn` ASC';		
			$result=mysqli_query($link,$sql);
		
			while ($games=mysqli_fetch_array($result,MYSQLI_ASSOC))
			{
				echo "<option value='".$games['key']."'>".utf8_encode($games['spelnamn'])."</option>";
			}
			print "</select>
			<br>
			<input type='submit' value='Öppna' name='oppna'>
			<input type='submit' value='Radera' name='del' onclick=\"return confirm('Du raderar ett spel, vill du fortsätta?')\" >
			</form>";
			
			
		?>
		</aside>
		<section class="main">
			<article>
				<p>
					<?

						print '<form method="post">
						<table>';
												
						if(isset($_POST['oppna']) && !empty($_POST['game']) || isset($_POST['upp']))
						{
							if(isset($_POST['upp']))
							{
								//Håller informationen öppen efter uppdatering.
								$sql = 'SELECT * FROM `spel` WHERE `spelnamn` = "'.$_POST['spelnamn'].'"';
							}
							else 
							{
								$sql = 'SELECT * FROM `spel` WHERE `key` = "'.$_POST['game'].'"';
							}
							
							$result=mysqli_query($link,$sql);
							$showgame=mysqli_fetch_array($result,MYSQLI_ASSOC);
						
							echo '<input type="hidden" name="key" value="'.$showgame["key"].'">';
							echo "<tr><td><small>Inlagd:</small></td><td><small>".substr($showgame["indatum"],0,10)."</small></tr></td>";
							echo "<tr><td><small>Senast ändrad:</small></td><td><small>".$showgame["editdatum"]."</small></tr></td>";
							
						}

						print '
						<tr>
						<td><label for="title">Titel:</label></td>
						<td><input type="text" name="spelnamn" value="'.utf8_encode($showgame["spelnamn"]).'" required></td>
						</tr><tr>
								<td><label for="title">URL:</label></td>
						<td><input type="url" name="lank" value="'.utf8_encode($showgame["lank"]).'"></td>
						</tr><tr>
						<td><label for="title">Ägare:</label></td>
						<td>'.listAgare($showgame["agare"]).'</td>
						</tr><tr>
						<td><label for="title">Genre:</label></td>
						<td>'.listTyp($showgame["typ"]).'</td>
						</tr><tr>
						<td><label for="title">Min-Spelare:</label></td>
						<td><input type="number" name="minspelare"  value="'.utf8_encode($showgame["minspelare"]).'" required></td>
						</tr><tr>
						<td><label for="title">Max-Spelare:</label></td>
						<td><input type="number" name="maxspelare" value="'.utf8_encode($showgame["maxspelare"]).'" required></td>
						</tr><tr>
						<td><label for="title">Speltid (min):</label></td>
						<td><input type="number" name="speltid" value="'.utf8_encode($showgame["speltid"]).'" required></td>
						</tr><tr>
						<td><label for="title">Co-op:</label></td>
						<td><input type="checkbox" name="co-op" value="'.utf8_encode($showgame["co-op"]).'" '.checkBox($showgame["co-op"]).'></td>
						</tr><tr>
						<td><label for="title">Finns i spelrum:</label></td>
						<td><input type="checkbox" name="ispelrum" value="'.utf8_encode($showgame["ispelrum"]).'" '.checkBox($showgame["ispelrum"]).'></td>
						</tr><tr>
						<td colspan="2"><input type="submit" value="Skapa/Uppdatera" name="upp"></td>
						</tr>
						
						</table></form>';
					?>
				
				</p>
			</article>
		</section>
		<?php include '../include/footer.inc'; ?>
	</body>
</html>

<?
function checkBox($checked)
{
	if($checked == "1")
	{
		return "checked";
	}
}	
function listAgare($choice)
{
	include "../../../private_html/link.inc";
	$sql = "SELECT `spelarnamn` FROM `deltagare` ORDER BY `spelarnamn` ASC";		
	$result=mysqli_query($link,$sql);
	
	$out="<select name='agare'>".preChoice($choice);
	while ($person=mysqli_fetch_array($result,MYSQLI_ASSOC))
	{
	$out.= "<option value='".$person['spelarnamn']."'>".$person['spelarnamn']."</option>"; //.utf8_encode($person).
	}
	return $out."</select>";
}

function listTyp($choice)
{
	return '
	<select name="typ">
	'.preChoice($choice).'
	<option value="Abstract Strategy">Abstract Strategy</option>
	<option value="Customizable">Customizable</option>
	<option value="Thematic">Thematic</option>
	<option value="Party">Party</option>
	<option value="Strategy">Strategy</option>
	<option value="Wargames">Wargames</option>
	</select>';
}
function preChoice($choice)
{
	$out = '<option value="'.$choice.'">'.$choice.'</option>';	
	return $out;
}
?>