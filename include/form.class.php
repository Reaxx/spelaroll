<?php

/**
 * Used to generate form-elements.
 * @author Reax
 */
class Form
{

    protected $method;

    function __construct($method = "POST", $id = null, $action = null)
    {
        $this->method = $method;
        echo "<form method=\"" . $this->method . "\">\n";
    }

    /**
     * Generates a input of text-type
     *
     * @param string $name
     * @param string $title    
     * @param string $value Start value. Default = null
     * @param bool $remember default = true. $value must be set if false.
     */
    public function text($name, $title, $value = null, $remember = true)
    {
        if ($remember) {
            $value = $this->remember($name, $value);
        }
        echo "<input type='text' name='$name' title='$title' value='$value'>\n";
    }

    /**
     * Generate "number", "email" and other unsual types.
     *
     * @param string $type
     * @param string $name
     * @param string $title
     * @param string $value Start value. Default = null
     * @param bool $remember default = true. $value must be set if false.
     */
    public function generic($type, $name, $title, $value = null,  $remember = true)
    {
        if ($remember) {
            $value = $this->remember($name, $value);
        }
        echo "<input type='$type' name='$name' title='$title' value='$value'>\n";
    }

    /**
     * Generats a button, default values -> submit.
     * @param string $text
     * @param string $name Optional, default = open
     * @param string $type Optional, default = submit.
     */
    public function button($text, $name = "open", $type = "submit")
    {
        echo "<button type='$type' name='$name'>$text</button>\n";
    }
    
    public function textarea()
    {
        
    }
    public function radiobutton()
    {
        
    }
    public function checkbox()
    {
        
    }

   
    /**
     * Makes the input "remember" earlier data
     * @param string $name
     * @param string $value Start value.
     * @return string Earlier entry 
     */
    private function remember($name, $value)
    {
        if ($this->method == "GET" && isset($_GET[$name])) {
            $var = $_GET[$name];
        } else if ($this->method == "POST" && isset($_POST[$name])) {
            $var = $_POST[$name];
        } else {
            $var = $value;
        }
        
        return $var;
    }

    /*
     * public static function textStatic($type, $name, $title, $remember, $value=null)
     * {
     * //Form::textStatic($type = 'post',$name='test', $title = 'test2', $remember = TRUE, $value=NULL);
     * $name = strtoupper($name);
     * if($remember == true)
     * {
     * $value = '$_'.$name.'["$name"]';
     * }
     * echo "<input type='text' name='$name' title='$title' value='$value'>\n";
     * }
     */
    function __destruct()
    {
        echo "</form>\n";
    }
}

?>