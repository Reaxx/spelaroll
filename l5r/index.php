<?php

    $diceRoller = strip_tags($_POST["name"]);
    $diceToRoll = $_POST["toRoll"];
    $diceType = 10; // $_POST["type"];
    $diceToKeep = $_POST["toKeep"];
    $valueToAdd = $_POST["toAdd"];
    
    $roll = array();
    $keep = array();
    $discard = array();
    $fileArray = array();
    
    $countdown =0;
    $rollResult = 0;
    $resultsText = "";
    $keptDiceHover = "";
    
    $errorMessage = array("<p class='errorText'>Please fill out the required fields correctly.</p>",
                          "<p class='errorText'>Let's stay within a thousand dice, shall we?</p>",
                          "<p class='errorText'>Duuude! Negative dice rolls!<br>Awesome, but not allowed.</p>",
                          "<p class='errorText'>You can't keep more dice than you roll!<br>Dumbass.</p>",
                          "<p class='errorText'>You've gotta keep yourself positive!<br>And only keep positive dice.</p>")
    
?>


<html>
	<head>
		<meta charset="UTF-8">
        <meta name="theme-color" content="#ffffff">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>L5R Dice Generator</title>
        <script src="roll.js"></script>

        <link rel="stylesheet" href="l5r.css">

        <link rel="apple-touch-icon" sizes="57x57" href="./fav/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="./fav/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="./fav/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="./fav/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="./fav/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="./fav/apple-touch-icon-120x120.png">
        <link rel="icon" type="image/png" href="./fav/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="./fav/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="./fav/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="./fav/manifest.json">
        <link rel="mask-icon" href="./fav/safari-pinned-tab.svg" color="#5bbad5">
    </head>

	<body>
    <header><img src="l5r.png" width="650"></header>
    <form name="L5Rdicegen" action="index.php" method="post" accept-charset="UTF-8">
    <div id="genDiv">
		<table id="mainTable">

			<tr>
				<td align="right"><label for="name">Name:</label></td>
				<td colspan="5"><input placeholder="Required" type="text" id="name" name="name" <?php if (isset($diceRoller)) {echo 'value="' .$diceRoller. '"';} ?> ></td>
			</tr>

			<tr>
				<td align="right"><label for="toRoll">Roll:</label></td>

                    <td><input placeholder="Required" type="number" id="toRoll" name="toRoll" min="1" maxlength="3" <?php if (isset($diceToRoll)) {echo 'value="' .$diceToRoll. '"';} ?> ></td>
                    <!--<td><label for="type">d</label></td>
                    <td><input placeholder="Required" type="number" id= "type"name="type" min="2" <?php if (isset($diceType)) {echo 'value="' .$diceType. '"';} /* else {echo 'value="10"';} */ ?> ></td> -->
                    <td><label for="toKeep">k</label></td>
					<td><input placeholder="Required" type="number" id="toKeep" name="toKeep" min="1" maxlength="3" <?php if (isset($diceToKeep)) {echo 'value="' .$diceToKeep. '"';} ?> ></td>
                    <td><label for="toAdd">+</label></td>
					<td><input placeholder="Optional" type="number" id="toAdd" name="toAdd" maxlength="3" <?php if (isset($valueToAdd)) {echo 'value="' .$valueToAdd. '"';} ?> ></td>

			</tr>

			<tr>
				<td align="right"></td><td colspan="5"><input type="submit" name="submit" value="Roll your dice"></td>
			</tr>

            <tr>
                <td align="right"></td><td colspan="5">





<?php
    
    // :: When the submit button is clicked ::
    
    if (isset($_POST["submit"])) {
    
        // :: Throw an error message if not all variables are set correctly; otherwise proceed with execution ::

        if (!(!empty($diceRoller) && is_numeric($diceToRoll) && is_numeric($diceType) && is_numeric($diceToKeep))) {
            echo $errorMessage[0];
            
        } elseif ($diceToRoll > 999) {
            echo $errorMessage[1];
            
        } elseif ($diceToRoll < 0) {
            echo $errorMessage[2];
            
        } elseif ($diceToKeep > $diceToRoll) {
            echo $errorMessage[3];
            
        } elseif ($diceToKeep < 0) {
            echo $errorMessage[4];
            
        } else {
            
            // :: Roll the dice, explode any 10s, and keep (and discard) the correct number of dice ::
            
            $index = 0;
            $countdown = $diceToRoll;
            while ($countdown > 0) {
                
                while($roll[$index] % 10 == 0) {
                    $roll[$index] = $roll[$index] + rand(1,$diceType);
                }
                $countdown--;
                $index++;
            }

            rsort($roll);
	
            $index = 0;
            $countdown = $diceToKeep;
            while ($countdown > 0) {
                $keep[$index] = $roll[$index];
                $rollResult = $rollResult + $keep[$index];
                $countdown--;
                $index++;
            }
            
            if ($diceToRoll != $diceToKeep) {
                
                sort($roll);
            
                $index = 0;
                $countdown = $diceToRoll - $diceToKeep;
                while ($countdown > 0) {
                    $discard[$index] = $roll[$index];
                    $countdown--;
                    $index++;
                }
            }
            
            rsort($discard);
            
            // :: Add roll details to $resultsText and print it on screen ::
            
            if (is_numeric($valueToAdd) && $valueToAdd != 0) {
                $rollResult = $rollResult + $valueToAdd;
                $resultsText = "<span class='bold'>{$diceRoller}</span> rolled {$diceToRoll}k{$diceToKeep}+{$valueToAdd} for a result of <span class='bold'>{$rollResult}</span>"; //<br>(using {$diceType}-sided dice)
                echo "<p class='resultsText'>", $resultsText, "</p>";
                
            } else {
                $resultsText = "<span class='bold'>{$diceRoller}</span> rolled {$diceToRoll}k{$diceToKeep} for a result of <span class='bold'>{$rollResult}</span>"; //<br>(using {$diceType}-sided dice)
                echo "<p class='resultsText'>", $resultsText, "</p>";
                
            }
            
            // :: Add dice details to $resultsText ::
            
            $resultsText = "<p class='timestamp'>" . time() . "</p><p class='rolltext'>" . $resultsText . "</p>";
            
            $keptDiceHover = "title='Kept dice: ";
            $iteration = 0;
            foreach ($keep as $dice) {
                $iteration++;
                if ($iteration == count($keep)) {
                    $keptDiceHover = $keptDiceHover . $dice . "";
                } elseif ($iteration == count($keep) - 1) {
                    $keptDiceHover = $keptDiceHover . $dice . " and ";
                } else {
                    $keptDiceHover = $keptDiceHover . $dice . ", ";
                }
            }
            if ($diceToRoll != $diceToKeep) {
                $keptDiceHover = $keptDiceHover . "&#013;Discarded dice: ";
                $iteration = 0;
                foreach ($discard as $dice) {
                    $iteration++;
                    if ($iteration == count($discard)) {
                        $keptDiceHover = $keptDiceHover . $dice . "'";
                    } elseif ($iteration == count($discard) - 1) {
                        $keptDiceHover = $keptDiceHover . $dice . " and ";
                    } else {
                        $keptDiceHover = $keptDiceHover . $dice . ", ";
                    }
                }
            } else {
                $keptDiceHover = $keptDiceHover . "'";
            }

            
            $resultsText = "<article " . $keptDiceHover . ">" . $resultsText . "</article>\n";

            // :: Get current history from file and save value of $resultsText to it (while keeping it to max 25 values) ::
            
            $resultsText = array($resultsText);
            $fileArray = array_merge($resultsText, file("history.txt"));
            while (count($fileArray) > 25) {
                array_pop($fileArray);
            }

            $file = fopen("history.txt", "w");
            foreach ($fileArray as $row) {
                fwrite($file, $row);
            }
            fclose($file);
        }
    }
?>


                    </td>
                </tr>

            </table>
            </div>

        <div id="backDiv">
            <div id="topDiv">
                <span id="chooseBackgroundSpan">
                    <select title="Choose affiliation" id="backgroundImageSelect" name="backgroundImageSelect" onchange="image(this);">
                        <option value=0>Serene White</option>
                        <option value=1>The Crab Clan</option>
                        <option value=2>The Crane Clan</option>
                        <option value=3>The Dragon Clan</option>
                        <option value=4>The Lion Clan</option>
                        <option value=5>The Mantis Clan</option>
                        <option value=6>The Phoenix Clan</option>
                        <option value=7>The Scorpion Clan</option>
                        <option value=8>The Spider Clan</option>
                        <option value=9>The Unicorn Clan</option>
                        <option value=10>Ronin</option>
                        <option value=11 selected>Okami Castle</option>
                        <option value=12>[OOC] Champloo</option>
                    </select>
                </span>

                <input type="submit" id="update" name="update" value="Update history">
            </div>
            <div id="resDiv">
                <?php
                
                    // :: Write out the contents of the history file ::
                
                    $fileArray = file("history.txt");
                    foreach ($fileArray as $row) {
                        echo $row;
                    }
                
                ?>

            </div>
        </div>
        </form>

        <?php
            $currentTime = time();
            echo "<span id='hiddenTimeNow'>" . $currentTime . "</span>";
        ?>

        <script type="text/javascript">

            loadBackground();
            timeAll();

            function timeAgo(stamp)
            {
                var diff = Math.floor(document.getElementById("hiddenTimeNow").innerHTML-stamp);
                var timeString = "Rolled ";
                var d = Math.floor(diff / 86400);
                var h = Math.floor(diff % 86400 / 3600);
                var m = Math.floor(diff % 86400 % 3600 / 60);
                var s = Math.floor(diff % 86400 % 3600 % 60);
                if(d>0)
                {
                    timeString+=d+"d ";
                }
                if(h>0)
                {
                    timeString+=h+"h ";
                }
                if(m>0)
                {
                    timeString+=m+"m ";
                }
                timeString+=s+"s ago";
                return timeString;
            }
        </script>
    </body>
</html>
