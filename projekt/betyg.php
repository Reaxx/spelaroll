
<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/style.css">
		<title>Betygsätt spel</title>
		
		<script type="text/javascript">
		function updateNumber(newNumber, str1)
		{
			document.getElementById(str1).value=newNumber;
		}
		</script>
	</head>
	<body>
		<?
			if(isset($_POST['setBetyg']))
			{
				$spelarnamn = $_POST['spelarnamn'];
				$maxQ = $_POST['Q'];
				$q = 0;
				while ($q < $maxQ)
				{
					$betyg = $_POST['betyg'.$q];
					$spelnamn = $_POST['spelnamn'.$q];
					skrivDB($betyg, $spelnamn, $spelarnamn);
										
					$q++;
				} 
				
			}
		?>
	
		<?php include './include/nav.inc'; ?>
		<aside>
			<article>
				<p>
				<?				
				include "/home/virtual/spelaroll.eu/private_html/link.inc";
				$sql = "SELECT `spelnamn`,`lank` FROM `spel` WHERE 1 ORDER BY `spelnamn` ASC";			
				$result=mysqli_query($link,$sql);
		
				echo "<form method='post'>";
				echo "<table>";
				
				if($_SERVER['PHP_AUTH_USER'] == "Admin")	//Låter admin sätta betyg för andra spelare. Hårdkodad, tillfällig lösning.
				{
					deltagare();					
				}
				else
				{
					echo "<tr><td colspan='3'><input type='text' value='".inloggad()."' disabled><br></td></tr>"; //Bara för att verifiera för användaren att de är på rätt namn
				}
				
				echo "<tr><td colspan='3'><select name='spel[]' multiple>";
		
				while ($spel=mysqli_fetch_array($result,MYSQLI_ASSOC))
				{
					
					echo "<option value='".$spel['spelnamn']."@".$spel['lank']."'>".utf8_encode($spel['spelnamn']);
					if (isBetygSet($spel['spelnamn'], inloggad()) != 0)
					{
						echo " - ".isBetygSet($spel['spelnamn'], inloggad())."";
					}
					echo "</option>";
					/**
					if (isBetygSet($spel['spelnamn'], inloggad()) != 0)
					{
						echo "<option value='".$spel['spelnamn']."@".$spel['lank']."'>".utf8_encode($spel['spelnamn'])."*</option>";
					}
					else
					{
						echo "<option value='".$spel['spelnamn']."@".$spel['lank']."'>".utf8_encode($spel['spelnamn'])."</option>";
					}
					**/
				}
				echo "</select></td></tr>";
				echo "<tr><td><input type='submit' value='Öppna' name='oppna'></td>";
				echo "<td>Dölj bilder:</td>";
				
				echo "<td><input type='checkbox' name='hideThumb' ".checkBox($_POST['hideThumb'])."></td></tr>";
									
				echo "</table></form>"; 				
				?>
				</p>
			</article>
		</aside>
		<section class="main">
			<article>
				<p>
				<?
				if(isset($_POST["oppna"]))
				{
					echo "<table><form method='post'>";
					//
					$q = 0;
					echo "<tr><td><input type='submit' value='Sätt betyg' name='setBetyg'></td></tr>";
					foreach ($_POST['spel'] as $val)
					{
						$lank = explode("@",$val)[1];
						$val = explode("@",$val)[0];
						
						//Sätter aktiv användare (framförallt för admin)
						if(isset($_POST['deltagare']))
						{
							$spelarnamn = $_POST['deltagare'];
						}
						else
						{
							$spelarnamn = inloggad();
						}							
						
						echo "<input type='hidden' value='".$spelarnamn."' name='spelarnamn'>"; //Skickar spelarnamn
						
						$sql = "SELECT * FROM `betyg` WHERE `spelnamn` = '".$val."' AND `spelarnamn` = '".$spelarnamn."'";
						$result=mysqli_query($link,$sql);											
						
						//Sätter start-värde för range, 5 som standard annars tidigare betyg.
						if($valtSpel=mysqli_fetch_array($result,MYSQLI_ASSOC))
						{
							$betyg = $valtSpel['betyg'];
						}
						else
						{
							$betyg = "2.5";
						}
						
						//För att JS-scriptet ska hitta rätt
						$jsVer = '"number'.$q.'"';

						echo "<input type='hidden' name='spelnamn".$q."' value='".$val."'>";
						echo "<tr><td colspan='2' rowspan='3'><a href='".$lank."' target='_blank'><img src='".getThumbUrl($lank,$_POST['hideThumb'])."' alt='Thumbnail' height='50em' width='50em'></a></td></tr>";
						echo "<tr><td colspan='2'>".utf8_encode($val)."</td></tr>";
						echo "<tr><td><input type='range' step='0.5' value='".$betyg."' name='betyg".$q."' min='1' max='5' onchange='updateNumber(this.value,".$jsVer.")'></td>";
						echo "<td><input type='text' id=".$jsVer." value='".$valtSpel['betyg']."' disabled size='1'></td></tr>";						
						$q++;
					}
				echo "<input type='hidden' name='Q' value='".$q."'>";
				echo "<tr><td><input type='submit' value='Sätt betyg' name='setBetyg'></td></tr>";
				echo "</form></table>";
				}
				
				
				?>
				</p>
			</article>
		</section>
		

	
		<?php include './include/footer.inc'; ?>
	</body>
</html>

<?
function skrivDB($betyg, $spelnamn, $spelarnamn)
{
	include "/home/virtual/spelaroll.eu/private_html/link.inc";
	$sql = "SELECT * FROM `betyg` WHERE `spelarnamn` = '".$spelarnamn."' AND spelnamn = '".$spelnamn."'";
	$result=mysqli_query($link,$sql);
	$betygDB = mysqli_fetch_array($result,MYSQLI_ASSOC);
	
	if($betygDB["betyg"] == $betyg)
	{
		//Entryn oförändrad, gör ingen uppdatering av databasen.
	}
	elseif(!empty($betygDB["betyg"]))
	{
		$sql = "UPDATE `betyg` SET `betyg` = '".$betyg."' WHERE `spelarnamn` = '".$spelarnamn."' AND `spelnamn` = '".$spelnamn."'";
		$result=mysqli_query($link,$sql);
	}
	else
	{
		$indatum = date("Y-m-d");
		$sql = "INSERT INTO `betyg` (`spelarnamn`, `spelnamn`, `betyg`, `indatum`) VALUES ('".$spelarnamn."', '".$spelnamn."', '".$betyg."', '".$indatum."');";
		$result=mysqli_query($link,$sql);
	}
}
//Kollar om betyg redan är sat, 1 = finns, 0 = finns inte.
function isBetygSet($spelnamn, $spelarnamn)
{
	include "/home/virtual/spelaroll.eu/private_html/link.inc";
	$sql = "SELECT * FROM `betyg` WHERE `spelnamn` = '".$spelnamn."' AND `spelarnamn` = '".$spelarnamn."'";
	$result=mysqli_query($link,$sql);
	if($valtSpel=mysqli_fetch_array($result,MYSQLI_ASSOC))
	{
		return $valtSpel['betyg'];
	}
	else
	{
		return 0;
	}
}
function inloggad()
{
	include "/home/virtual/spelaroll.eu/private_html/link.inc";
	$user = $_SERVER['PHP_AUTH_USER'];
	$sql = "SELECT * FROM `deltagare` WHERE `user` = '".$user."'";
	$result=mysqli_query($link,$sql);
	while ($inloggad=mysqli_fetch_array($result,MYSQLI_ASSOC))
	{
		return $inloggad["spelarnamn"];
	}
}

function deltagare()
{
	//Generar lista på alla deltagare
	include "/home/virtual/spelaroll.eu/private_html/link.inc";
	echo "<select name='deltagare'>";		
	$sql = 'SELECT * FROM `deltagare` ORDER BY `spelarnamn` ASC';		
	$result=mysqli_query($link,$sql);
	
	if(isset($_POST["deltagare"]))
	{
			echo "<option value='".$_POST["deltagare"]."'>".utf8_encode($_POST["deltagare"])."</option>";
	}
	
	while ($deltagare=mysqli_fetch_array($result,MYSQLI_ASSOC))
	{
		echo "<option value='".$deltagare['spelarnamn']."'>".utf8_encode($deltagare['spelarnamn'])."</option>";
	}
	echo "</select>";
}

/** Hämtar BGGs thumb-nail utifrån länk **/
function getThumbUrl($url,$off)
{
	if($off != "on")
	{
	//Plockar fram IDt från BGG-länk 
	//Exempel: https://boardgamegeek.com/boardgame/68448/7-wonders
	$url = explode ("/",$url);
	$id = $url[4];

	//Hämtar information om spelet från BGG
	$curl = curl_init("http://boardgamegeek.com/xmlapi/boardgame/".$id);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	
	//Plockar ut thumb-nail URLen från BGG
	$imgUrl = explode ("//",$result);
	$parsedImgUrl = strip_tags($imgUrl[2]);
	$parsedImgUrl = chop($parsedImgUrl);
	return "http://".$parsedImgUrl;
	}
}
function checkBox($checked)
{
	if($checked == "on")
	{
		return "checked";
	}
}
?>