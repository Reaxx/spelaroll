<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="style.css">
		<script src="code.js"></script>
		<title>Admin</title>
	</head>
	<body>
		<?php include './include/nav.inc'; ?>
		<header class="title">
			<h1>Administration</h1>	
		</header>
		<aside>
			<article>
				<p>
					Saknar du åtmkomst till sidor du borda ha tillgång till så kontakta Jonny 
				</p>
			</article>
		</aside>
		<section class="main">
			<article>
				<table>
				<tr><td><a href="./restricted/mod/adnews.php">Uppdatera Nyheter</a></td><tr>
				<tr><td><a href="./restricted/mod/adevent.php">Uppdatera Event</a></td><tr>
				<tr><td><a href="./restricted/admin/adspel.php">Uppdatera speldatabasen</a></td><tr>
				<tr><td><a href="./restricted/admin/addeltagare.php">Uppdatera deltagardatabasen</a></td><tr>
				<tr><td><a href="./restricted/users/betyg.php">Sätta betyg på spel</a></td><tr>
				</table>
			</article>
		</section>
		<?php include './include/footer.inc'; ?>
	</body>
</html>